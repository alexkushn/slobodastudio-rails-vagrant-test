$databases = ['vagrant_test_development']
$as_vagrant   = 'sudo -u vagrant -H bash -l -c'
$home         = '/home/vagrant'

$ruby_version = '2.1.2'

include apt

Exec {
  path => ['/usr/sbin', '/usr/bin', '/sbin', '/bin']
}

user { 'alex':
  ensure => present,
  gid => 'admin',
}

stage { 'preinstall':
  before => Stage['main']
}

class apt_get_update {
  exec { 'apt-get -y update':
    unless => "test -e ${home}/.rvm"
  }
}
class { 'apt_get_update':
  stage => preinstall
}

class install_postgres {
  apt::source { 'postgresql':
    location   => 'http://apt.postgresql.org/pub/repos/apt',
    release    => "precise-pgdg",
    key        => 'ACCC4CF8',
    key_source => 'https://www.postgresql.org/media/keys/ACCC4CF8.asc',
  }

  class { 'postgresql':
    version => '9.3'
  }

  class { 'postgresql::server': }

  pg_database { $databases:
    ensure   => present,
    encoding => 'UTF8',
    require  => Class['postgresql::server']
  }

  pg_user { 'alex':
    ensure  => present,
    require => Class['postgresql::server']
  }

  pg_user { 'vagrant':
    ensure    => present,
    superuser => true,
    require   => Class['postgresql::server']
  }

  package { 'libpq-dev':
    ensure => installed
  }

  package { 'postgresql-contrib':
    ensure  => installed,
    require => Class['postgresql::server'],
  }
}
class { 'install_postgres': }

package { 'curl':
  ensure => installed
}

package { 'htop':
  ensure => installed
}

package { 'git-core':
  ensure => installed
}

package { ['libxml2', 'libxml2-dev', 'libxslt1-dev']:
  ensure => installed
}

package { 'nodejs':
  ensure => installed
}

exec { 'install_rvm':
  command => "${as_vagrant} 'curl -L https://get.rvm.io | bash -s stable'",
  creates => "${home}/.rvm/bin/rvm",
  require => Package['curl']
}

exec { 'install_ruby':
  command => "${as_vagrant} '${home}/.rvm/bin/rvm install ruby-${ruby_version} --binary --autolibs=enabled && rvm alias create default ${ruby_version}'",
  creates => "${home}/.rvm/bin/ruby",
  require => Exec['install_rvm']
}

exec { "install_bundler":
  command => "${as_vagrant} 'gem install bundler --no-rdoc --no-ri'",
  creates => "${home}/.rvm/bin/bundle",
  require => Exec['install_ruby']
}

exec { 'install_gems':
  command => "${as_vagrant} 'cd /vagrant && bundle install --local'",
  require => Exec['install_bundler']
}

exec { 'update-locale':
  command => 'update-locale LANG=en_US.UTF-8 LANGUAGE=en_US.UTF-8 LC_ALL=en_US.UTF-8'
}
